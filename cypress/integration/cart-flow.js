context("Adding and Removing To/From Cart", () => {

  beforeEach(() => {
    cy.visit("/uryndyklar/cart")
    
    cy.get("main > div > div")
      .children()
      .should("have.length", 0)
  })

  afterEach(() => {
    cy.get("main > div > div").children().should("not.exist")
    
  })

  it("From Main Page", () => {
    let c = 0
    cy.visit("/uryndyklar")
    cy.get("button")
      .each(($el, _index, _list) => {
        if($el.find("span").text() == "Add to cart") {
          cy.wrap($el).click({ force: true })
          c++
        }
      }).then(() => {
        cy.get('header')
          .contains("Cart")
          .click()
          
        for(let i = c; i > 0; i--) {
          cy.get("main > div > div")
          .children()
          .should("have.length", i)
        
          cy.get("main button")
            .first().click()
        }
      })
    
    
  })

  it("From Catalog Page", () => {
    let c = 0
    cy.visit("/uryndyklar/catalog").wait(3000)
    cy.get("button")
      .each(($el, _index, _list) => {
        if($el.find("span").text() == "Add to cart") {
          cy.wrap($el).click({ force: true })
          c++
        }
      }).then(() => {
        cy.get('header')
        .contains("Cart")
        .click()
        
        for(let i = c; i > 0; i--) {
          cy.get("main > div > div")
          .children()
          .should("have.length", i)
        
          cy.get("main button")
            .first().click()
        }
      })
    
    
  })


})