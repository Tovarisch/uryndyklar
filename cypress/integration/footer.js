describe("Checking Footer", () => {
    beforeEach(() => {
        cy.visit('/uryndyklar')
    })

    it("Link to Main Page", () => {
        let testingEmail = "testing@testing.com"
        cy.get('footer')
          .get('input[type="email"]')
          .type(testingEmail)
          .should("have.value", testingEmail)
    })
})