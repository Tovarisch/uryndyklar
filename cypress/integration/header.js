const linksToCheck = [
    ["Main", "uryndyklar"],
    ["About Us", "/about-us"],
    ["Catalog", "/catalog"],
    ["Cart", "/cart"]
]

describe("Checking Header", () => {
    beforeEach(() => {
        cy.visit('/uryndyklar')
    })

    linksToCheck.forEach(
        (lst) => {
            let pageName = lst[0]
            let subUrl = lst[1]
            it("Link to " + pageName + " Page", () => {
                let header = cy.get('header')
                header.contains(pageName).click()
                header.url().should('include', subUrl)
            })
        }
    )
})