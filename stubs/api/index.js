const router = require("express").Router();

// The code in this folder is partially based on the code from this repo: 
// https://bitbucket.org/online-mentor/auth-system/src/master/stubs/api/

const wait = (time = 2000) => (req, res, next) => setTimeout(next, time);

const stubs = [
    ['cart',   'get',  'success'],
	// ['search', 'get',  'success'],
	['cart',   'post', 'success'],
]


for (let [func, method, mock] of stubs) {
	router[method](`/${func}`, wait(), (req, res) => {
	    res.send(require(`./mocks/${func}_${method}/${mock}`))
	});
}

router.get('/search', wait(), (req, res, _) => {
	data = require('./mocks/search_get/success.json')
	items = data.items
	const filters = req.query.filters.length > 0 ? req.query.filters.split(',') : []
	const query = req.query.query.toLowerCase()

	const chairsToSend = []
	for(const chair of items) {
		if(chair.model.toLowerCase().includes(query) && filters.every(tag => chair.tags.includes(tag))){
			chairsToSend.push(chair)
		}
	}

	res.send(
		{
			"success": true,
			"items": chairsToSend
		}
	)
	

})



module.exports = router;
