const pkg = require('./package')

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    "navigations": {
        "uryndyklar": "/uryndyklar",
        "uryndyklar.about-us": "/uryndyklar/about-us",
        "uryndyklar.catalog": "/uryndyklar/catalog",
        "uryndyklar.cart": "/uryndyklar/cart",
    },
    "config": {
        "uryndyklar.api": "/api/",
    },
    "features": {
        'uryndyklar': {
            'cart': true,
            'signup': true,
        },
    },
};
