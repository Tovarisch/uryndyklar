import styled from 'styled-components'

const AppWrapper = styled.div`
    font-size: 1.2em;
    &, h1, h2, h3, h4, h5, h6 {
        font-family: Ubuntu, Arial, sans-serif;
    }

    display: flex;
    flex-direction: column;
    min-height: 100vh;
    main {
        flex: 1;
    }
`

export {AppWrapper}