import { api_root } from '../__data__/constants/urls'
import axios from 'axios'

export const apiAxios = axios.create({
    baseURL: api_root,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
    },
});
