import { getFeatures } from '@ijl/cli';

const features = getFeatures('uryndyklar');

export default features;
