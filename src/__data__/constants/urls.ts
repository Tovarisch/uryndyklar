import {getNavigations, getConfigValue} from '@ijl/cli';


const navigation = getNavigations('uryndyklar');

const urls = {
    main: navigation['uryndyklar'],
    about: navigation['uryndyklar.about-us'],
    catalog: navigation['uryndyklar.catalog'],
    cart: navigation['uryndyklar.cart']
}

const api_root = getConfigValue("uryndyklar.api")
export {urls, api_root};
