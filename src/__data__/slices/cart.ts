import { createSlice, createSelector, PayloadAction } from "@reduxjs/toolkit"
import type { RootState } from "../store"
import { CartItem } from "../models"

type CartState = {
    items: CartItem[]
}

const initialState: CartState = {
    items: []
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action: PayloadAction<CartItem>) => {
            const newCartItem: CartItem = action.payload
            state.items.push(newCartItem)
        },

        removeFromCart: (state, action: PayloadAction<number>) => {
            // removes nth item from the cart specified in the payload
            state.items.splice(action.payload, 1)
        },

        clearCart: (state) => {
            state.items = []
        }
    },
})

export const { addToCart, removeFromCart, clearCart } = cartSlice.actions

export const selectCartItems = createSelector((state: RootState) => state, state => state.cart.items)
export default cartSlice.reducer
