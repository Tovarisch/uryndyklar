import { createSlice, createSelector } from "@reduxjs/toolkit"

const slice = createSlice({
  name: 'chairsRequest',
  initialState: {
    chairs: {items: []},
    error: null,
    loading: false
  },
  reducers: {
    fetch(state){
      state.loading = true
      state.error   = null
    },
    success(state, action){
      state.loading = false
      state.error = null
      state.chairs = action.payload
    },
    error(state, action){
      state.loading = false
      state.error = action.payload
    }
  }
})

export const { fetch, success, error } = slice.actions
export const selectChairs = state => state.chairsRequest
export default slice.reducer
