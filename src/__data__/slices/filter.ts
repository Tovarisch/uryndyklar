import { createSlice, createSelector } from "@reduxjs/toolkit"

export const filterSlice = createSlice({
    name: 'filter',
    initialState: {
        items: [],
        query: ''
    },
    reducers: {
        addFilter: (state, action) => {
            const newFilter = action.payload
            state.items.push(newFilter)
        },

        setQuery: (state, action) => {
            state.query = action.payload
        },        

        removeFilter: (state, action: {type:string, payload:string}) => {
          const index = state.items.indexOf(action.payload);
          state.items.splice(index, 1)
        },

        clearFilter: (state) => {
            state.items = []
        }
    },
})

export const { addFilter, removeFilter, clearFilter, setQuery } = filterSlice.actions
export const selectFilters = state => state.filter.items
export const selectQuery = state => state.filter.query
export default filterSlice.reducer
