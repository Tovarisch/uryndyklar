export type CartItem = {
    id: string;
    model: string
    price_usual: string
    price_discounted?: string
    seller?: string
    img: string
}
