import { fetch, success, error } from '../slices/chairsRequest'
import { apiAxios } from '../../util'

export const apiRequestChairs = (params, error_handler=null) => (dispatch) => {
    
    if (!error_handler) {
        error_handler = (msg) => console.error('Api fetch error: ' + msg)
    }
    
    dispatch(fetch())
    apiAxios({
        method: 'get',
        url: 'search',
        params: {
            filters: params.filters.join(','),
            query: params.query
        },
    })
    .then((response) => {
        const data = response.data
        if (!data['success']) {
            dispatch(error('Got error message' + data['message']))
            error_handler(data['message'] || 'Unknown error')
            return
        }
        dispatch(success(response.data))
    })
    .catch((err) => {
        error_handler(err)
        dispatch(error(err.toString()))
    })
    
}
