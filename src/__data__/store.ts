// import { createStore } from 'redux'
import cartReducer from './slices/cart'
import filterReducer from './slices/filter'
import chairsRequestReducer from './slices/chairsRequest'

import { configureStore } from '@reduxjs/toolkit'

const store = configureStore({
  reducer: {
    cart: cartReducer,
    filter: filterReducer,
    chairsRequest: chairsRequestReducer
  }
})

export type RootState = ReturnType<typeof store.getState>
export default store;
