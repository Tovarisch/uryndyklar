import styled from 'styled-components'

const Icon = styled.img`
    height: ${props => props.size || "1.5em"};
  	margin: 0 ${props => props.offset || "0"};
  	vertical-align: middle;
`

export default Icon
