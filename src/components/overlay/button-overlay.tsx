import React from 'react'

import Overlay from './overlay'
import { TextButton } from '../button'

type ButtonOverlayProps = {
  only_on_hover?: boolean;
  onClick: () => void;
  text: string;
  style?: any;
}

const ButtonOverlay: React.FC<ButtonOverlayProps> = ({only_on_hover, style, onClick, text}) => (
  <Overlay only_on_hover={only_on_hover} style={style}>
    <TextButton
          onClick  = {onClick}
          text     = {text}
      />
  </Overlay>
)

export default ButtonOverlay
