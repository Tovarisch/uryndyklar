import styled, {css} from 'styled-components';

const Overlay = styled.div`
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 2;
    *:hover > & {
        display: block;
    }

    ${props => props.only_on_hover &&
    css`
        & {
            display: none;
        }

        &:hover + * {
            opacity: 0.5;
        }
    `}
`

export default Overlay