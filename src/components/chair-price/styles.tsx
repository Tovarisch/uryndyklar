import styled from 'styled-components'

const ChairTextPriceUsual = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: flex-start;
    flex-basis: 50%;
`
const ChairPriceUsualSpan = styled.span`
    font-size: 1.15em;
    margin-top: 0.4em;
    margin-right: 0.5em;
    text-decoration: line-through;
    color: rgb(120, 120, 120);
`


const ChairTextPriceDiscounted = styled.div`
    display: flex;
    align-items: flex-end;
    flex-basis: 50%;
`
const ChairPriceDiscountedSpan = styled.span`
    font-size: 1.5em;
    margin-bottom: 0.3em;
    margin-left: 0.25em;
    color: rgb(230, 55, 70);
`

const TextPriceContainer = styled.div`
    display: flex;
    flex-basis: 55%;
    justify-content: center;
`

export {
    TextPriceContainer, ChairTextPriceUsual, ChairPriceUsualSpan,
    ChairTextPriceDiscounted, ChairPriceDiscountedSpan
}