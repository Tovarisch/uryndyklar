import React from 'react'
import {
    TextPriceContainer, ChairTextPriceUsual, ChairPriceUsualSpan,
    ChairTextPriceDiscounted, ChairPriceDiscountedSpan
} from "./styles"


type ChairPriceProps = {
    price_usual: string;
    price_discounted: string;
}

const ChairPrice: React.FC<ChairPriceProps> = ({price_usual, price_discounted}) => {
    return (
        <TextPriceContainer>
            <ChairTextPriceUsual>
                <ChairPriceUsualSpan>
                    {price_usual}
                </ChairPriceUsualSpan>
            </ChairTextPriceUsual>
            
            <ChairTextPriceDiscounted>
                <ChairPriceDiscountedSpan>
                    {price_discounted}
                </ChairPriceDiscountedSpan>
            </ChairTextPriceDiscounted>
        </TextPriceContainer>
    )
}

export default ChairPrice