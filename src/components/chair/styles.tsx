import {OverlayWrapper} from "../overlay"
import styled, { css } from 'styled-components'
import features from "../../__data__/constants/features"

const ChairItem = styled(OverlayWrapper)`
	width: 230px;
	flex-grow: 0;
	flex-basis: 230px;
	height: 500px;
	border-radius: 15px;
	display: flex;
	flex-direction: column;
	font-family: Ubuntu;
`

const ChairImgContainer = styled.div`
	background-color: white;
	display: flex;
	flex-basis: 75%;
	align-items: center;
	justify-content: center;
	border: 3px solid rgb(224, 224, 224);
	border-radius: 1em;
`

const ChairImg = styled.img`
	height: auto;
	max-width: 100%;

	${ features['cart'] && css`
		${ChairImgContainer}:hover & {
			background: light-gray;
			opacity: 0.5;
		}`
	}

`
export {ChairItem, ChairImgContainer, ChairImg}
