import React from 'react'

import { useDispatch }  from 'react-redux'
import {useTranslation} from "react-i18next"

import ChairInfo from '../chair-info'
import { ButtonOverlay } from '../overlay'

import {ChairItem, ChairImgContainer, ChairImg} from "./styles"

import { addToCart } from '../../__data__/slices/cart'
import { CartItem } from '../../__data__/models'
import features from "../../__data__/constants/features"

type ChairProps = {
    id: string;
    img: string;
    price_usual: string;
    price_discounted?: string;
    button_text?: string;
    model: string;
    tags?: string[];
}

const Chair: React.FC<ChairProps> = ({id, img, price_usual, price_discounted, button_text, model, tags}) => {
    const {t} = useTranslation()
    const dispatch = useDispatch()
    const newCartItem: CartItem = {
        id,
        img,
        model,
        price_usual,
        price_discounted
    }
    const handleAddToCart = () => {
        dispatch(addToCart(newCartItem))
    }
    return (
        <ChairItem>
            <ChairImgContainer>
                { features['cart'] &&
                    <ButtonOverlay
                        text = {button_text || t('uryndyklar.button.addToCart')}
                        only_on_hover = {true}
                        onClick = {handleAddToCart}
                    />
                }
                <ChairImg src={img}/>
            </ChairImgContainer>
            <ChairInfo
                price_usual={price_usual}
                price_discounted={price_discounted}
                model = {model}>
            </ChairInfo>
        </ChairItem>
    )
}

Chair.defaultProps = {
    id: '0',
    img: 'https://ehire.co.za/wp-content/uploads/2018/10/furniture-chairs-conference-chair-charcoal.jpg',
    price_usual: '$199',
    price_discounted: '$99',
    button_text: null,
    model: "",
    tags: []
}

export default Chair;
