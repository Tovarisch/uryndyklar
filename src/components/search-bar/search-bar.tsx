import React, {useState} from 'react'
import {SearchContainer, SearchInput} from "./styles"
import { SearchButton } from '../button'
import { useDispatch, useSelector } from 'react-redux'
import { selectFilters, setQuery, selectQuery } from "../../__data__/slices/filter"
import { apiRequestChairs } from '../../__data__/actions/catalog'


const SearchBar: React.FC = () => {
    const dispatch = useDispatch()
    
    const filters = useSelector(selectFilters)
    const query = useSelector(selectQuery)
    const handleInput = (event) => {
        dispatch(setQuery(event.target.value))
    }


    const search = (event) => {
        dispatch(apiRequestChairs({filters, query}))
    }


    return(
        <SearchContainer>
            <SearchInput onChange={ handleInput } value={query}/>
            <SearchButton onClick={ search }/>
        </SearchContainer>
    )
}

export default SearchBar
