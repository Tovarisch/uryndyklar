import styled from 'styled-components'

const HorizontalList = styled.div`
    max-width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-around;

    & > * {
    	flex-basis: 0;
    	flex-grow: 1;
    }
`

export default HorizontalList
