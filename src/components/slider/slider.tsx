import React, {useState} from 'react'

import styled from 'styled-components';
import Skeleton from "react-loading-skeleton"
import {useTranslation} from "react-i18next"

import { slider_image } from '../../assets'
import { ButtonOverlay, OverlayWrapper } from '../overlay'
import features from "../../__data__/constants/features"

const SliderImg = styled.img`
    width: 100%;
`

const Slider = () => {
    const [imageLoaded, setImageLoaded] = useState(false)
    const {t} = useTranslation()
    const handleImageLoaded = () => {
        setImageLoaded(true)
    }
    return (
        <OverlayWrapper>
            { !imageLoaded && <Skeleton height={'56.25vw'}/> }
            { features['cart'] &&
                <ButtonOverlay
                    text = {t('uryndyklar.button.buyNow')}
                    style = {{
                        right: '10%',
                        top: 'min(70vh, 80%)'
                    }}
                    onClick = {() => {alert('TODO')}}
                />
            }
            <SliderImg src={slider_image} hidden={!imageLoaded} onLoad={handleImageLoaded} />
        </OverlayWrapper>
    )
}


export default Slider
