import React from 'react'
import {Label} from "./styles"
import { useDispatch, useSelector }  from 'react-redux'
import { addFilter, removeFilter, selectFilters } from '../../__data__/slices/filter'


type CheckboxProps = {
    id: string,
    text: string
}

const Checkbox: React.FC<CheckboxProps> = ({id, text}) => {
    const dispatch = useDispatch()
    const handleFiltering = (event) => {
        if (!filters.includes(id)) {
            dispatch(addFilter(id))
        }
        else {
            dispatch(removeFilter(id))
        }
    }

    const filters = useSelector(selectFilters)

    return (
        <div>
            <input type='checkbox' id={id} onChange = {handleFiltering} checked={filters.includes(id)}/>
            <Label htmlFor={id}> {text} </Label>
        </div>
    )
   
    
    
}

export default Checkbox
