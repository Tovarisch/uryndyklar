import React from 'react'
import { NavItemWrapper, HeaderContainer, HeaderElement, LogoContainer, NavElement } from "./styles"

import { main_logo } from "../../assets"

import { Link } from "react-router-dom";
import Icon from '../icon'

import { urls } from "../../__data__/constants/urls"
import features from "../../__data__/constants/features"
import { useTranslation } from "react-i18next"


type NaveItemProps = {
    url: string;
    text: string;
}

const NavItem: React.FC<NaveItemProps> = ({url, text}) => {
    return (
        <NavItemWrapper>
            <span>
                <Link to={ url }>{ text }</Link>
            </span>
        </NavItemWrapper>
    )
}

const Header = () => {
    const {t} = useTranslation()

    const navItems = []
    navItems.push(<NavItem key="main"    text={t('uryndyklar.header.title')}   url={urls.main}    />)
    navItems.push(<NavItem key="about"   text={t('uryndyklar.header.aboutUs')} url={urls.about}   />)
    navItems.push(<NavItem key="catalog" text={t('uryndyklar.header.catalog')} url={urls.catalog} />)
    if (features['cart']) {
        navItems.push(<NavItem key="cart" text={t('uryndyklar.header.cart')} url={urls.cart} />)
    }

    return (
        <HeaderElement>
            <HeaderContainer>
                <LogoContainer>
                    <Link to={urls.main}>
                        <Icon src={ main_logo } />
                        <span>uryndyklar</span>
                    </Link>
                </LogoContainer>
                
                <NavElement>
                    {...navItems}
                </NavElement>
            </HeaderContainer>
        </HeaderElement>  
    )
}


export default Header
