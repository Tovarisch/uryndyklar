import styled from 'styled-components'


const HeaderElement = styled.header`
    border-bottom: 2px solid black;
    a {
        color: black;
        text-decoration: none;
    }
`

const HeaderContainer = styled.div`
    max-width: 100%;
    padding: 0.5em;
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: space-around;
`

const LogoContainer = styled.div`
    margin-left: 1em;
    font-size: 1.5em;
    a {
        display: flex;
        align-items: center;
    }
`

const NavElement = styled.nav`
    width: 100%;
    height: 1.5em;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    a:hover {
        text-decoration: underline;
    }
`

const NavItemWrapper = styled.div`
    margin-right: 1em;
    margin-left: 1em;
`

export {NavItemWrapper, HeaderContainer, HeaderElement, LogoContainer, NavElement}