import styled from 'styled-components'

const ChairTextName = styled.div`
    display: flex;
    flex-basis: 40%;
    justify-content: center;
    align-items: center;
`

const ChairNameSpan = styled.span`
    font-size: 1.35em;
`
export {ChairTextName, ChairNameSpan}