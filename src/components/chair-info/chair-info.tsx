import React from 'react'
import ChairPrice from '../chair-price'
import {ChairTextName, ChairNameSpan} from "./styles"

type ChairInfoProps = {
    model: string;
    price_usual: string;
    price_discounted: string;
}

const ChairInfo: React.FC<ChairInfoProps> = ({model, price_usual, price_discounted}) => {
    return (
        <div>
            <ChairTextName>
                <ChairNameSpan>
                    { model }
                </ChairNameSpan>
            </ChairTextName>
            <ChairPrice price_usual={ price_usual } price_discounted={ price_discounted }/>
         </div>
    )
}


export default ChairInfo
