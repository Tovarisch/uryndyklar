import React from 'react'
import {FooterContainer, FooterElement, SignUpContainer, SignUpContainerInput} from "./styles"
import { twitter_logo } from "../../assets"
import { facebook_logo } from "../../assets"
import { instagram_logo } from "../../assets"
import { youtube_logo } from "../../assets"

import HorizontalList from '../horizontal-list'
import Icon from '../icon'
import { TextButton } from '../button'

import features from "../../__data__/constants/features"
import {useTranslation} from "react-i18next"



const Footer = () => {
    const {t} = useTranslation()
    return (
        <FooterElement>
            <FooterContainer>
                <HorizontalList>
                    <Icon src={twitter_logo}   offset="1em" size="2em" />
                    <Icon src={facebook_logo}  offset="1em" size="2em" />
                    <Icon src={instagram_logo} offset="1em" size="2em" />
                    <Icon src={youtube_logo}   offset="1em" size="2em" />
                </HorizontalList>
                { features['signup'] &&
                    <SignUpContainer>
                        <SignUpContainerInput type="email" placeholder={t('uryndyklar.footer.enterEmail')}/>
                        <TextButton text={t('uryndyklar.footer.signUp')} onClick = {() => {alert('TODO')}} />
                    </SignUpContainer>
                }
            </FooterContainer>
        </FooterElement>
    )
}

export default Footer
