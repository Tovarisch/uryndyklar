import styled from 'styled-components'

const FooterElement = styled.footer`
    background-color: black;
`

const FooterContainer = styled.div`
    max-width: 100%;
    padding: 0.5em;
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: space-around;
    padding-left: 3em;
`

const SignUpContainer = styled.div`
    background-color: white;
    margin-left: auto;
    margin-right: 2em;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    border-radius: 10px;
`

const SignUpContainerInput = styled.input`
    font-size: 1.3em;
    text-align: center;
    border-radius: 0px;
    border: none;
    outline:none!important;
`

export {FooterContainer, FooterElement, SignUpContainer, SignUpContainerInput}