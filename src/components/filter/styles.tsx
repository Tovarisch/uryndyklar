import styled from 'styled-components'

const FilterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    background: #732083;
    border-radius: 33px;
    height: fit-content;
`

const Container = styled.div`
    margin: 1em
`
const Header = styled.h2`
    margin: 0.5em;
    color: #FFFFFF;
    align: left;
`

export {Container, FilterWrapper, Header}