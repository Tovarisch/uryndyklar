import React from 'react'
import {Container, FilterWrapper, Header} from "./styles"
import Checkbox from '../checkbox'
import {useTranslation} from "react-i18next"



const Filter = () => {
    const {t} = useTranslation()
    return(
        <FilterWrapper>
            <Container>
                <Header>{t('uryndyklar.filter.type.name')}</Header>
                <Checkbox id='office' text={t('uryndyklar.filter.type.office')}/>
                <Checkbox id='gamer' text={t('uryndyklar.filter.type.gamer')}/>
                <Checkbox id='for_children' text={t('uryndyklar.filter.type.forChildren')}/>
            </Container>
            <Container>
                <Header>{t('uryndyklar.filter.maker.name')}</Header>
                <Checkbox id='secretlab' text='Secretlab'/>
                <Checkbox id='ikea' text='IKEA'/>
                <Checkbox id='dx_racer' text='DX-Racer'/>
                <Checkbox id='chairman' text='Chairman'/>
                <Checkbox id='argos' text='Argos'/>
            </Container>
            <Container>
                <Header>{t('uryndyklar.filter.color.name')}</Header>
                <Checkbox id='green' text={t('uryndyklar.filter.color.green')}/>
                <Checkbox id='blue' text={t('uryndyklar.filter.color.blue')}/>
                <Checkbox id='red' text={t('uryndyklar.filter.color.red')}/>
                <Checkbox id='white' text={t('uryndyklar.filter.color.white')}/>
                <Checkbox id='black' text={t('uryndyklar.filter.color.black')}/>
                <Checkbox id='pink' text={t('uryndyklar.filter.color.pink')}/>
                <Checkbox id='multi_color' text={t('uryndyklar.filter.color.multiColor')}/>
                <Checkbox id='yellow' text={t('uryndyklar.filter.color.yellow')}/>
                <Checkbox id='brown' text={t('uryndyklar.filter.color.brown')}/>
            </Container>
            <Container>
                <Header>{t('uryndyklar.filter.material.name')}</Header>
                <Checkbox id='dark_oak' text={t('uryndyklar.filter.material.darkOak')}/>
                <Checkbox id='acacia' text={t('uryndyklar.filter.material.acacia')}/>
                <Checkbox id='maple' text={t('uryndyklar.filter.material.maple')}/>
                <Checkbox id='spruce' text={t('uryndyklar.filter.material.spruce')}/>
                <Checkbox id='birch' text={t('uryndyklar.filter.material.birch')}/>
                <Checkbox id='oak' text={t('uryndyklar.filter.material.oak')}/>
                <Checkbox id='jungle' text={t('uryndyklar.filter.material.jungle')}/>
            </Container>
        </FilterWrapper>
    )
}
export default Filter
