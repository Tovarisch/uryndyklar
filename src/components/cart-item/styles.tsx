import styled from 'styled-components'

const CartItemElement = styled.div`
    width: 30%;
    height: 130px;
    padding: 1em 1em;
    border-radius: 15px;
    display: flex;
    font-family: Ubuntu;
    background-color: white;
    overflow: hidden;
    border: 1px solid rgb(224, 224, 224);
    border-radius: 1em;
    margin: 1em;
`

const ChairImgContainer = styled.div`
    background-color: white;
	display: flex;
	align-items: center;
	justify-content: center;
	border: 3px solid rgb(224, 224, 224);
	border-radius: 1em;
    flex-basis: 20%;
    width: 20%;
    flex-shrink: 1;
`

const ChairImg = styled.img`
    width: 80%;
    height: auto;
`

const ChairInfoContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 1em;
    flex-shrink: 3;
`

const ChairInfoName = styled.div`
    font-size: 1.5em;
`

const ChairInfoSeller = styled.div`
    margin-top: 1.3em;
    color: rgb(120, 120, 120);
`

const ChairInfoSellerSpan = styled.span`
    color: black;
`

const ChairPriceRemoveContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;
    margin-left: auto;
`

const RemoveItemContainer = styled.div`
    display: flex;
    align-items: center;
    height: 30%;
`
export {
    CartItemElement, ChairImgContainer, ChairImg, ChairInfoContainer,
    ChairInfoName, ChairInfoSeller, ChairInfoSellerSpan, ChairPriceRemoveContainer,
    RemoveItemContainer
}