import React from 'react'
import {remove_icon} from "../../assets"
import ChairPrice from '../chair-price'
import {Button} from '../button'
import Icon from '../icon'
import {useTranslation} from "react-i18next"
import { useDispatch } from 'react-redux'
import { removeFromCart } from '../../__data__/slices/cart'
import {
    CartItemElement, ChairImgContainer, ChairImg, ChairInfoContainer,
    ChairInfoName, ChairInfoSeller, ChairInfoSellerSpan, ChairPriceRemoveContainer,
    RemoveItemContainer
} from "./styles"



const RemoveButton = () => {
    const {t} = useTranslation()
    return (
        <RemoveItemContainer>
            <Icon src={remove_icon} size="1.5em" />
            {t('uryndyklar.button.remove')}
        </RemoveItemContainer>
    )
}

type CartItemProps = {
    list_index: number,
    id: string,
    img?: string,
    price_usual: string,
    price_discounted?: string,
    model?: string,
    seller?: string
}

const CartItem : React.FC<CartItemProps> = (props) => {
    const {t} = useTranslation()
    const dispatch = useDispatch()
    const handleRemove = () => {
        dispatch(removeFromCart(props.list_index))
    }
    return (
        <CartItemElement>
            <ChairImgContainer>
                <ChairImg src={props.img} />
            </ChairImgContainer>

            <ChairInfoContainer>
                <ChairInfoName>
                    <ChairInfoSellerSpan>
                        {props.model}
                    </ChairInfoSellerSpan>
                </ChairInfoName>
                <ChairInfoSeller>
                    {t('uryndyklar.filter.maker.name')}: <ChairInfoSellerSpan>{props.seller}</ChairInfoSellerSpan>
                </ChairInfoSeller>
            </ChairInfoContainer>

            <ChairPriceRemoveContainer>
                <Button
                    font_size="0.7em"
                    onClick = {handleRemove}
                >
                    <RemoveButton />
                </Button>

                <ChairPrice price_usual={ props.price_usual } price_discounted={ props.price_discounted }/>
            </ChairPriceRemoveContainer>
        </CartItemElement>
    )
}

CartItem.defaultProps = {
    img: 'https://ehire.co.za/wp-content/uploads/2018/10/furniture-chairs-conference-chair-charcoal.jpg',
    price_usual: '$199',
    price_discounted: '$99',
    model: "Test model",
    seller: "IKEA",
}
export default CartItem;
