import React from 'react'
import Button from './button'
import { search_icon } from '../../assets'
import Icon from '../icon'

type SearchButtonProps = {
    onClick: (event) => void;
}

const SearchButton: React.FC<SearchButtonProps> = ({onClick}) => {
    return (
        <Button 
        onClick={onClick} 
        color="#732083" 
        border_radius="4px" 
        padding="0">

                <Icon src={search_icon} size="80%"/>

        </Button>
    )
}

export default SearchButton
