import styled from 'styled-components'

const Button = styled.button`
    font-size: ${props => props.font_size || "1em"};
    padding: ${props => props.padding || "0.5em 2em"};
    border-radius: ${props => props.border_radius || "10px"};
    background-color: ${props => props.color || "rgb(230, 55, 70)"};
    text-align: center;
    color: white;
    outline: none;
    border: none;
    cursor: pointer;
    &:not([disabled]):active > * {
        position: relative;
        bottom: -1px;
    }
    display: flex;
    align-items: center;
    justify-content: center;
`

export default Button
