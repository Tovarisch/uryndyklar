import React from 'react'

import Button from './button'

type TextButtonProps = {
  onClick: () => void;
  text: string;
}

const TextButton: React.FC<TextButtonProps> = ({onClick, text}) => (
  <Button onClick={ onClick }><span>{ text }</span></Button>
)

export default TextButton
