import main_logo from "./icons/main_logo.png"
import search_icon from "./icons/search.png"
import slider_image from "./images/chairgame.jpeg"
import remove_icon from "./icons/remove.svg"

import twitter_logo from "./icons/social/twitter.png"
import facebook_logo from "./icons/social/facebook.png"
import instagram_logo from "./icons/social/instagram.png"
import youtube_logo from "./icons/social/youtube.png"

import phone_icon from "./icons/contact/phone.png"
import email_icon from "./icons/contact/email.png"

import delivery_icon from "./icons/qualities/delivery.png"
import rating_icon from "./icons/qualities/rating.png"
import customer_icon from "./icons/qualities/customer.png"
import chair_icon from "./icons/qualities/chair.png"
import tools_icon from "./icons/qualities/tools.png"

export {
	// other
	main_logo,
    search_icon,
    slider_image,
    remove_icon,

    // socials
    twitter_logo,
    facebook_logo,
    instagram_logo,
    youtube_logo,

    // contacts
    phone_icon,
    email_icon,

    // qualities
    delivery_icon,
    rating_icon,
    customer_icon,
    chair_icon,
    tools_icon,
}

