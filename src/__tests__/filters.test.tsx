import React from "react";
import { mount } from "enzyme";
import { describe, expect, it, beforeEach} from "@jest/globals";
import { Provider } from 'react-redux'
import store from '../__data__/store'

import { MainPage, CartPage, CatalogPage } from "../pages"
import Chair from "../components/chair"
import type { CartItem } from "../__data__/models"

import MockAdapter from 'axios-mock-adapter'
import { apiAxios } from '../util'
const search_success_response = require('../../stubs/api/mocks/search_get/success.json')
const modelH_response = require('../../stubs/api/mocks/search_get/modelH.json')
const ikea_response = require('../../stubs/api/mocks/search_get/ikea.json')
const ikea_multicolor_response = require('../../stubs/api/mocks/search_get/ikea_multicolor.json')

let apiMock
beforeEach(async () => {
	apiMock = new MockAdapter(apiAxios)
})

describe('Test catalog', () => {

	it('Test filters and search', done => {
		// providing mock for replying on api requests
		apiMock.onGet("search", { params: { filters: '', query: '' } }).reply(200, search_success_response)
		apiMock.onGet("search", {
			params: {
				filters: 'ikea',
				query: ''
			}
		}).reply(200, ikea_response)
		apiMock.onGet("search", {
			params: {
				filters: 'ikea,multi_color',
				query: ''
			}
		}).reply(200, ikea_multicolor_response)
		apiMock.onGet("search", {
			params: {
				filters: 'ikea,multi_color',
				query: 'model H'
			}
		}).reply(200, modelH_response)

		// user goes to catalog pages
		const catalog = mount(<Provider store = {store}><CatalogPage /></Provider>)

		// user clicks on IKEA chairs to filter chairs
		catalog.find('Filter input#ikea').simulate('change')

		// user waits for the completion of query
		setTimeout(() => {
			catalog.update()

			// there should be only two chairs with names 'model M' and 'model H'
			const chairs = catalog.find('Chair')
			expect(chairs.length).toEqual(2)
			expect(chairs.at(0).props()['model']).toEqual('model M')
			expect(chairs.at(1).props()['model']).toEqual('model H')

			// user clicks on Multicolor chairs to filter chairs
			catalog.find('Filter input#multi_color').simulate('change')

			// user writes model H query in the search bar
			const searchInput = catalog.find('SearchBar input')
			searchInput.simulate('change', { target: { value: 'model H' } })

			// user clicks the search button
			const searchButton = catalog.find('SearchBar button')
			searchButton.simulate('click')

			// user waits for the completion of query
			setTimeout(() => {
				catalog.update()

				// there should be only one chair with name 'model H'
				const chairs = catalog.find('Chair')
				expect(chairs.length).toEqual(1)
				expect(chairs.first().props()['model']).toEqual('model H')

				done()
			}, 100)
		}, 100)

	});

})
