import React from "react";
import { mount } from "enzyme";
import { describe, expect, it } from "@jest/globals";
import { Provider } from 'react-redux'
import store from '../__data__/store'

import { MainPage, AboutUsPage, CatalogPage, CartPage, NotFoundPage } from "../pages"
import Header from "../components/header"
import Footer from "../components/footer"

const elementsToTest = [
	["Main page", MainPage],
	["About Us page", AboutUsPage],
	["Catalog page", CatalogPage],
	["Cart page", CartPage],
	["404 page", NotFoundPage],
	["Header", Header],
	["Footer", Footer]
];

describe('test rendering', () => {
	it.each(elementsToTest)(
		'%s renders as expected',
		(elementName, TestedElement) => {
			const component = mount(
				<Provider store = {store}>
					<TestedElement />
				</Provider>
			)
			expect(component).toMatchSnapshot()
		}
	);
})