import React from "react";
import { mount } from "enzyme";
import { describe, expect, it, beforeEach} from "@jest/globals";
import { Provider } from 'react-redux'
import store from '../__data__/store'
import { clearCart } from '../__data__/slices/cart'

import { MainPage, CartPage, CatalogPage } from "../pages"
import Chair from "../components/chair"
import type { CartItem } from "../__data__/models"

import MockAdapter from 'axios-mock-adapter'
import { apiAxios } from '../util'
const search_success_response = require('../../stubs/api/mocks/search_get/success.json')

let cart, apiMock
beforeEach(async () => {
	store.dispatch(clearCart())
	cart = mount(<Provider store = {store}><CartPage /></Provider>)
	apiMock = new MockAdapter(apiAxios)
})

describe('Test cart for adding/removing items', () => {

	it('Adding random chairs', () => {

		const chairs = search_success_response.items
		const testChairs = []
		for (const chair of chairs.slice(0, 2)) {
				const chairElement = mount(<Provider store = {store}>
												<Chair
													id               = {chair.id}
													key              = {chair.id}
													img              = {chair.picture}
													price_discounted = {chair.price_discounted}
													price_usual      = {chair.price_usual}
													model            = {chair.model}
													tags             = {chair.tags}>
												</Chair>
											</Provider>)
				testChairs.push(chairElement)
		}
		testChairs[0].find('button').simulate('click')
		testChairs[1].find('button').simulate('click')
		testChairs[1].find('button').simulate('click')
		cart.update()
		expect(cart).toMatchSnapshot()
	});

	it('Adding chairs from main page', () => {
		const main = mount(<Provider store = {store}><MainPage /></Provider>)
		const buttons = main.find('Chair ButtonOverlay button')
		buttons.forEach(button => button.simulate('click'));
		cart.update()
		expect(cart).toMatchSnapshot()
	});

	it('Removing chairs', done => {

		// user goes to catalog page
		apiMock.onGet("search", { params: { filters: '', query: '' } }).reply(200, search_success_response)
		const catalog = mount(<Provider store = {store}><CatalogPage /></Provider>)
		expect(cart).toMatchSnapshot() // should have loader

		// user waits for chairs to load
		setTimeout(() => {
			// user clicks on 'add to cart' button on 5 chairs
			catalog.update()
			const buttons = catalog.find('Chair ButtonOverlay button')
			for (let i = 0; i < 5; i++) {
				buttons.at(i).simulate('click')
			}

			// user deletes items from cart
			cart.update()
			for (const index of [4, 0, 1, 0, 0]) {
				cart.find('CartItem button').at(index).simulate('click')
				cart.update()
				expect(cart).toMatchSnapshot()
			}

			done()
		}, 2000)

	});

})
