import React, { useState, useEffect } from "react"

import Loader from "react-loader-spinner"

import { useDispatch, useSelector } from 'react-redux'

import { selectFilters, clearFilter, selectQuery } from "../../__data__/slices/filter"
import { selectChairs } from "../../__data__/slices/chairsRequest"

import {PageWrapper, ItemsWrapper} from "./styles"
import Chair from '../../components/chair'
import Filter from '../../components/filter'
import SearchBar from '../../components/search-bar'

import { apiRequestChairs } from "../../__data__/actions/catalog"

const CatalogPage: React.FC = () => {
    const dispatch = useDispatch()
    const catalog = useSelector(selectChairs) 
    const filters = useSelector(selectFilters)
    const query = useSelector(selectQuery)
    useEffect(() => {
        dispatch(apiRequestChairs({filters, query}))
    }, [filters])


    let renderChairs
  
    if(!catalog.loading){
        renderChairs = []
        for(const chair of catalog.chairs.items) {
            const render = <Chair
                                id={chair.id}
                                key={chair.id}
                                img={chair.picture}
                                price_discounted={chair.price_discounted}
                                price_usual={chair.price_usual}
                                model={chair.model}
                                tags={chair.tags}>
                            </Chair>

            renderChairs.push(render)
        }
    }
    else {
        renderChairs = (
            <Loader type="Circles"
                    color="#732083"
                    height={100}
                    width={100}
                    timeout={3000}
            />) 
    }
    
    {
      
        return(
            <div>
                <PageWrapper>
                    <Filter/>
                    <div style={ {width: '100%'} }>
                        <SearchBar/>
                        <ItemsWrapper>
                            { renderChairs }
                        </ItemsWrapper>
                    </div>
                </PageWrapper>
            </div>
        )
    }    
}


export default CatalogPage
