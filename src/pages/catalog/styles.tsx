import styled from "styled-components"

const PageWrapper = styled.div`
    display: flex;
    margin: 1em;
    `

const ItemsWrapper = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr;
    grid-gap: 1em;
    margin: 1em;
    max-width: 100%;
    align-items: center;
`

export {PageWrapper, ItemsWrapper}