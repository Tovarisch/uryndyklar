import React from 'react'
import {Link} from "react-router-dom";

const NotFoundPage = () => {
    return (
        <div style={{margin: "2em"}}>
            <h2>Page not found</h2>
            <p>We are sorry, but the page you requested is not here.</p>
            <p>You can return to the <Link to="/">main page</Link> or see our <Link to="/catalog">catalog</Link></p>
        </div>
    )
}

export default NotFoundPage
