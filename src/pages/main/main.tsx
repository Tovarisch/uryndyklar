import React from 'react'
import {useTranslation} from "react-i18next"
import {CenteredHeader, HorizontalSeparator, QualityWrapper, QualityCaption, QualityImage} from "./styles"
import Chair from '../../components/chair'
import Slider from '../../components/slider'
import HorizontalList from '../../components/horizontal-list'
import {delivery_icon, rating_icon, customer_icon, chair_icon, tools_icon} from '../../assets'


type QualityProps = {
    src: string;
    text: string;
}


const Quality: React.FC<QualityProps> = ({src, text}) => {
    return(
        <QualityWrapper>
            <QualityImage size="5em" src={src}/>
            <QualityCaption>{text}</QualityCaption>
        </QualityWrapper>
    )
}


const MainPage = () => {
    const {t} = useTranslation()
    return(
        <div>
            <Slider />
            <CenteredHeader>{t('uryndyklar.pages.main.bestsellers')}</CenteredHeader>
            <HorizontalList>
                <Chair
                  id = '1'
                  price_usual='$199'
                  price_discounted='$99'
                  img='https://ehire.co.za/wp-content/uploads/2018/10/furniture-chairs-conference-chair-charcoal.jpg'
                  model='Model 1'>
                </Chair>
                <Chair
                  id = '2'
                  price_usual='$599'
                  price_discounted='$399'
                  img='https://ehire.co.za/wp-content/uploads/2018/10/furniture-chairs-conference-chair-charcoal.jpg'
                  model='Model 2'>
                </Chair>
                <Chair
                  id = '3'
                  price_usual='$10'
                  price_discounted='$1'
                  img='https://ehire.co.za/wp-content/uploads/2018/10/furniture-chairs-conference-chair-charcoal.jpg'
                  model='Model 3'>
                </Chair>
            </HorizontalList>
            <HorizontalSeparator width="90%" size="0.15em" />
            <HorizontalSeparator width="80%" size="0.1em" />
            <HorizontalList>
                <Quality src={delivery_icon} text={t('uryndyklar.qualities.delivery')} />
                <Quality src={rating_icon}   text={t('uryndyklar.qualities.rating')} />
                <Quality src={customer_icon} text={t('uryndyklar.qualities.customer')} />
                <Quality src={chair_icon}    text={t('uryndyklar.qualities.chair')} />
                <Quality src={tools_icon}    text={t('uryndyklar.qualities.tools')} />
            </HorizontalList>
        </div>
    )
}

export default MainPage
