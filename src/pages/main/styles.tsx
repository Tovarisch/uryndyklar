import Icon from '../../components/icon'
import styled from 'styled-components'

const CenteredHeader = styled.h2`
    text-align: center;
`

const HorizontalSeparator = styled.hr`
    border: none;
    border-top: ${props => props.size} black solid;
`

const QualityWrapper = styled.div`
    text-align: center;
    margin: 4em 0;
`

const QualityCaption = styled.span`
    display: block;
    font-weight: bold;
    margin-top: 1em;
`

// should convert from black to needed color
// rgb(230, 55, 70) AKA #e63746
// generated with with https://codepen.io/sosuke/pen/Pjoqqp
const QualityImage = styled(Icon)`
    filter: invert(26%) sepia(65%) saturate(2758%) hue-rotate(336deg) brightness(96%) contrast(88%);
`

export {CenteredHeader, HorizontalSeparator, QualityWrapper, QualityCaption, QualityImage}