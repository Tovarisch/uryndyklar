import MainPage from './main/main'
import AboutUsPage from './about-us/about-us'
import CatalogPage from './catalog/catalog'
import NotFoundPage from './not-found'
import CartPage from './cart/cart'

export {
	MainPage,
	AboutUsPage,
	CatalogPage,
	CartPage,
	
	NotFoundPage
}
