import React from "react"
import CartItem from "../../components/cart-item"
import {PageWrapper} from "./styles"
import { useSelector } from "react-redux"
import { selectCartItems } from "../../__data__/slices/cart"


const CartPage : React.FC = () => {
    const items = useSelector(selectCartItems);
    
    const render_items = []
    for(const [i, item] of items.entries()){
        const render_item = <CartItem key={i}
                                      id={item.id}
                                      list_index={i}
                                      price_usual={item.price_usual}
                                      price_discounted={item.price_discounted}
                                      seller={item.seller}
                                      model={item.model}
                                      img={item.img}
                            />
        render_items.push(render_item)
    }
    
    return (
        <div>
            <PageWrapper>
                {render_items}
            </PageWrapper>
        </div>
    )
}

export default CartPage
