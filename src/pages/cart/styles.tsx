import styled from "styled-components"


const PageWrapper = styled.div`
    display: flex;
    padding: 1em;
    flex-flow: column;
    justify-content: center;
    align-items: center;
`

export {PageWrapper}