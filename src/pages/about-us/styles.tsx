import styled from 'styled-components'

const SectionHeader = styled.h2`
    text-decoration: underline;
    margin-top: 2em;
    margin-bottom: 0.5em;
`

const MapHeader = styled.h3`
    text-align: center;
    margin: 0;
    margin-bottom: 0.5em;
`

const MapIframe = styled.iframe`
    height: 15em;
    width: 30vw;
    border: 1px black solid;
`


export {SectionHeader, MapIframe, MapHeader}