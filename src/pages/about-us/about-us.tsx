import React from 'react'
import {useTranslation} from "react-i18next"
import {SectionHeader, MapIframe, MapHeader} from "./styles"
import { phone_icon } from '../../assets'
import { email_icon } from '../../assets'

import HorizontalList from '../../components/horizontal-list'
import Icon from '../../components/icon'


type MapProps = {
    name: string;
    point: string;
}

const Map: React.FC<MapProps> = ({name, point}) => {
    return (
        <div>
            <MapHeader>{ name }</MapHeader>
            <MapIframe src={`https://www.google.com/maps/embed?pb=${point}`} allowFullScreen="" loading="lazy"></MapIframe>
        </div>
    )
}

type FAQProps = {
    q: string;
    a: string;
}

const FAQ: React.FC<FAQProps> = ({q, a}) => {
    return (
        <div>
            <h3>{ q }</h3>
            <p>{ a }</p>
        </div>
    )
}



const AboutUsPage = () => {
    const {t, i18n} = useTranslation()
    let langstr = ''
        langstr += i18n.languages.length
        for (const [i, lang] of i18n.languages.entries()) {
            langstr += '!'
            langstr += i+1
            langstr += 's'
            langstr += lang
        }

        return (
            <div style={{margin: "2em"}}>
                <SectionHeader>{t("uryndyklar.pages.aboutUs.contactUs")}</SectionHeader>
                <div><Icon src={phone_icon} offset="0.25em" /> +7 401 418 3365</div>
                <div><Icon src={email_icon} /> uryndyklar@gmail.com</div>

                <SectionHeader>{t("uryndyklar.pages.aboutUs.pickUpPoints")}</SectionHeader>
                <HorizontalList>
                    <Map name={t("uryndyklar.pages.aboutUs.innopolis")} point={`!1m18!1m12!1m3!1d2245.5842357707315!2d48.739746215828255!3d55.74835258055214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x415bfe5cee411d5f%3A0x37152e0c54940240!2sSportivnaya%20St%2C%20108%2C%20Innopolis%2C%20Respublika%20Tatarstan%2C%20420500!5e0!3m${langstr}!4v1618580269556!5m${langstr}`} />
                    <Map name={t("uryndyklar.pages.aboutUs.moscow")}    point={`!1m18!1m12!1m3!1d561.1955661000774!2d37.6252359292439!3d55.76228449878472!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a5ce20a1ca1%3A0x2d0c87d028a7c988!2sUlitsa%20Kuznetskiy%20Most%2C%2019%2C%20Moskva%2C%20107031!5e0!3m${langstr}!4v1618580385989!5m${langstr}`} />
                    <Map name={t("uryndyklar.pages.aboutUs.spb")}       point={`!1m18!1m12!1m3!1d999.7048080955583!2d30.360939223745525!3d59.925344998215024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469631b09a67a47d%3A0x5be736d49df1c2ec!2zTGlnb3Zza3kgQXZlLCA1MCDQutC-0YDQv9GD0YEgMywgU2Fua3QtUGV0ZXJidXJnLCAxOTEwNDA!5e0!3m${langstr}!4v1618580421557!5m${langstr}`} />
                </HorizontalList>

                <SectionHeader>FAQ</SectionHeader>
                <FAQ q={t("uryndyklar.pages.aboutUs.question") + " 1?"} a="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus semper nibh nec diam dapibus egestas. Ut eget scelerisque nibh. In malesuada erat ut enim mattis pretium. Donec vitae justo est. Fusce dictum placerat ultricies. Quisque ac turpis eu libero pulvinar facilisis. Phasellus egestas pretium leo. Quisque quis ornare augue. Vestibulum sit amet elit a ex finibus dapibus. Curabitur semper diam vehicula massa aliquet, vitae malesuada sem venenatis. Pellentesque eget tincidunt ante. Donec tempor quam nec mi dapibus scelerisque. Vivamus sed tristique tellus, nec euismod libero. Suspendisse consequat massa justo, eu fermentum ante laoreet quis." />
                <FAQ q={t("uryndyklar.pages.aboutUs.question") + " 2?"} a="Nullam vitae augue a mauris consectetur ullamcorper. Aliquam vel dolor ut diam hendrerit rutrum. Nulla ut porttitor est. Nunc a feugiat eros. Aliquam vitae tellus ac mauris blandit volutpat sit amet a diam. Nam vitae diam massa. Aenean mollis commodo magna, eget dictum arcu fermentum ut. Phasellus ac molestie ex, sed dignissim urna. Sed aliquet sollicitudin pretium. Etiam ut malesuada diam, sed luctus ipsum. Aenean pharetra nisl eu semper varius. Duis tellus tellus, auctor sit amet mi ac, aliquam rutrum urna. Quisque ornare est ac elit ullamcorper, in efficitur quam tempor. In ultrices vehicula arcu sit amet tincidunt. Phasellus elementum, velit mattis commodo consequat, nibh sapien mattis risus, eget vestibulum ipsum ex a nulla." />
                <FAQ q={t("uryndyklar.pages.aboutUs.question") + " 3?"} a="Nulla ac laoreet purus, sit amet aliquet urna. Morbi sit amet convallis nisl, quis vulputate nisi. Proin et leo pretium, accumsan tortor id, euismod urna. Ut facilisis leo mi, a molestie nisi feugiat eu. Nulla rhoncus sollicitudin euismod. Nullam pharetra viverra nunc, sit amet rhoncus dolor iaculis et. Nulla facilisi. Vivamus volutpat ligula orci, vitae congue mi euismod sed. Maecenas tristique eros sem, et porta lacus cursus nec. Maecenas nunc nulla, gravida et ante id, interdum sodales ipsum. Sed consequat lobortis purus, quis eleifend nisi. Curabitur lacinia massa ac velit convallis, eget accumsan eros pharetra." />
                <FAQ q="Et cetera" a="Maecenas dictum vitae sapien vel feugiat. Quisque iaculis eros purus, vitae posuere lacus commodo et. Nulla aliquet nulla est, id. Nam in pharetra purus. Quisque." />
            </div>
        )
}



export default AboutUsPage
