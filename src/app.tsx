import React from 'react'
import {AppWrapper} from "./styles"
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Header from './components/header'
import Footer from './components/footer'
import {MainPage, AboutUsPage, CatalogPage, NotFoundPage, CartPage} from './pages'

import {urls} from './__data__/constants/urls'
import features from "./__data__/constants/features"

const App = () => {
    const routes = []
    routes.push(<Route exact path={urls.main}    component={MainPage}    />)
    routes.push(<Route exact path={urls.about}   component={AboutUsPage} />)
    routes.push(<Route exact path={urls.catalog} component={CatalogPage} />)
    if (features['cart']) {
        routes.push(<Route exact path={urls.cart} component={CartPage} />)
    }
    routes.push(<Route component={NotFoundPage} />)

    return(
        <AppWrapper>
            <BrowserRouter>
                <Header />
                <main>
                    <Switch>
                        {...routes}
                    </Switch>
                </main>
                <Footer />
            </BrowserRouter>
        </AppWrapper>
    )
}


export default App;

