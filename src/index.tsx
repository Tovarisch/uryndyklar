import React from 'react';
import ReactDom from 'react-dom';
import i18next from 'i18next';
import { i18nextReactInitConfig } from '@ijl/cli';

i18next.t = i18next.t.bind(i18next);

const i18nextPromise = i18nextReactInitConfig(i18next);

import App from './app';
export default () => <App/>;

import { Provider } from 'react-redux'
import store from './__data__/store'

export const mount = async (Component) => {
    await Promise.all([i18nextPromise]);
    ReactDom.render(
      <Provider store = {store}>
        <Component/>
      </Provider>,
        document.getElementById('app')
    );

    if((module as any).hot) {
        (module as any).hot.accept('./app', () => {
            ReactDom.render(
              <Provider store = {store}>
                <Component/>
              </Provider>,
                document.getElementById('app')
            );
        })
    }
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};
